# Configuration file for the Sphinx documentation builder.

# -- Project information -----------------------------------------------------

project = 'sphinx_test'
author = 'Yannick Chistel'
version = '1.1'

# -- General configuration ---------------------------------------------------

extensions = []

templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
